/*global google*/
import React, { Component } from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  DirectionsRenderer
} from "react-google-maps";
import {Row,
Col,Button
} from "reactstrap";
import Geocode from "react-geocode";

class MapRoute extends Component {
  state = {
    directions: null,
    origin: null,
    destination: null,
    time:null
  };

  componentDidMount() {
    Geocode.setApiKey("AIzaSyDms0AAI98NfOeZTqcTVgStPtAytmf3XGI");
    Geocode.setLanguage("en");


    // const origin = { lat: 40.756795, lng: -73.954298 };
    // const destination = { lat: 41.756795, lng: -78.954298 };

    this.getLocation(this.props.origin, this.props.destination);
  }

  getLocation(origin,destination){
    Geocode.fromAddress(origin).then(
        response => {
          const { lat, lng } = response.results[0].geometry.location;
          let location ={
              lat:lat,
              lng:lng
          }
          this.setState({origin :location })
        },
        error => {
          console.error(error);
        }
      );
      Geocode.fromAddress(destination).then(
        response => {
          const { lat, lng } = response.results[0].geometry.location;
          let location ={
              lat:lat,
              lng:lng
          }
          this.setState({destination :location });
        },
        error => {
          console.error(error);
        }
      );
  }

  componentDidUpdate(){
    const directionsService = new google.maps.DirectionsService();
        directionsService.route(
      {
        origin: this.state.origin,
        destination: this.state.destination,
        travelMode: google.maps.TravelMode.DRIVING
      },
      (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
            time:result.routes[0].legs[0].duration
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      }
    );
  }

  render() {
    const GoogleMapExample = withGoogleMap(props => (
      <GoogleMap
        defaultCenter={{ lat: 30.566991, lng: 7.857358 }}
        defaultZoom={2}
      >
        <DirectionsRenderer
          directions={this.state.directions}
        />
      </GoogleMap>
    ));

    let time = null;
        if (this.state.time){
           time = this.state.time.text 
        }
    return (
      <div>
          <Row>
              <Col>
          <GoogleMapExample
          containerElement={<div style={{ height: `700px`, width: "800px" }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
        </Col>
        <Col>
        <Button
                              block
                              color="primary"
                              onClick={() => this.notify("tc")}
                            >
                              {time}
                            </Button>
            
        </Col>
          </Row>
        
      </div>
    );
  }
}

export default MapRoute;
