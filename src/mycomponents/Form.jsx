import React from "react";

import {
  FormGroup,
  Label,
  Input,
  FormText,
  Row,
  Col,
  Button
} from "reactstrap";
import MapRoute from "./DirectionRenderer";
class Forms extends React.Component  {
    constructor(props){
super(props);
        this.state ={
            origin:"Zurich",
            destination:"Geneva"
        }
        this.handleChange =this.handleChange.bind(this);
    }


    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }
    rendermap(){
        
    }

render(){
    
  return (
      <React.Fragment>
    <form>
    <Row>
    <Col>
      <FormGroup>
        <Label for="current-address">Current address</Label>
        <Input
          name="origin"
          id="txtStartingPoint"
          placeholder="Enter Current Address"
          onChange={this.handleChange}
        />
        <FormText color="muted">
          We'll never share your location with anyone.
        </FormText>
      </FormGroup>
      </Col>
      <Col>
      <FormGroup>
        <Label for="destination-location">Destination Location</Label>
        <Input
          name="destination"
          id="txtDestinationPoint"
          placeholder="Destination Location"
          onChange={this.handleChange}
        />
      </FormGroup>
      </Col>
      </Row>    
      <Button id="btnQuery" value="Query" onClick={this.rendermap()}> Search</Button>
    </form>
    <MapRoute origin={this.state.origin} destination={this.state.destination}/>
    </React.Fragment>
  );
}
};

export default Forms;