/*!

=========================================================
* Paper Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import axios from "axios";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Container
} from "reactstrap";



class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      foo: "bar",
      eventData: {}
    };
  }

  getEventData() {
    axios
      .get("/event.json")
      .then(({ data }) => {
        this.setState({ eventData: data });
      })
      .catch(err => {
        console.log(err);
        alert(err);
      });
  }
  
  componentWillMount(){
    this.getEventData();
  }

  render() {
    const {eventData} = this.state;
    console.log(eventData);
    let work = null;
    if (eventData.length > 0)
  {
    work =eventData.map((event,index) => (
      <div key={event.company} >
        
          <Card style={{"padding":"15px"}}>

          <div>{event.title_en}</div>
          <div>{event.end_time}</div>
          <div>{event.date}</div>
          </Card>
      </div>));
  }
    // console.log(eventData);

    return (
      <>
        <div className="content">
          {work}
        </div>
      </>
    );
  }
}

export default Tables;
